import base64
import os
from pathlib import Path
from shutil import copyfile

from PIL import Image, ImageOps
from flask import Flask, render_template, url_for, Response, send_file
from flask_frozen import Freezer

app = Flask(__name__)
app.config['FREEZER_DESTINATION'] = 'public'
app.config['PICTURE_LOC'] = Path('pictures')
app.config['PICTURE_SRC'] = Path(app.root_path, 'resources', app.config['PICTURE_LOC'])
app.config['PICTURE_DIR'] = Path(app.root_path, app.static_folder, app.config['PICTURE_LOC'])
app.config['THUMB_LOC'] = app.config['PICTURE_LOC'] / 'thumbs'
app.config['THUMB_DIR'] = Path(app.root_path, app.static_folder, app.config['THUMB_LOC'])
app.config['ALREADY_COMPRESSED_PICTURES'] = [
    # newer pictures will be compressed to WebP
    "009.JPG",
    "DSC03079.JPG",
    "DSC03080.JPG",
    "DSC03082.JPG",
    "DSC03499.JPG",
    "DSC05005.pdf-000.jpg",
    "DSCF0041.JPG",
    "DSCF0044.JPG",
    "DSCF0046.JPG",
    "DSCF0049.JPG",
    "DSCF0055.JPG",
    "DSCF0056.JPG",
    "DSCF0057.JPG",
    "DSCF0066.JPG",
    "DSCI0047.pdf-000.jpg",
    "DSC_0028.pdf-000.jpg",
    "DSC_0030.JPG",
    "DSC_0032.JPG",
    "DSC_0052.jpg",
    "DSC_0081.pdf-000.jpg",
    "DSC_0085.pdf-000.jpg",
    "DSC_0100.pdf-000.jpg",
    "DSC_0105.pdf-000.jpg",
    "DSC_0106.pdf-000.jpg",
    "DSC_0109.pdf-000.jpg",
    "DSC_0113.pdf-000.jpg",
    "DSC_0118.pdf-000.jpg"
]
app.config['PHOTO_URL_PATH'] = '/kontakt/foto.jpg'
app.config['JSON_LD'] = {
    "@context": "http://schema.org",
    "@type": "LocalBusiness",
    "name": "Architekturbüro Eichler",
    "telephone": "+49337525530",
    "email": "mail@architektin-eichler.de",
    "url": "https://architektin-eichler.de",
    "image": f"https://architektin-eichler.de{app.config['PHOTO_URL_PATH']}",
    "address": {
        "@type": "PostalAddress",
        "streetAddress": "Ahornweg 4",
        "addressLocality": "Königs Wusterhausen",
        "postalCode": "15711",
        "addressRegion": "Brandenburg",
        "addressCountry": "Deutschland"
    }
}

os.makedirs(app.config['PICTURE_DIR'] / 'thumbs', exist_ok=True)
freezer = Freezer(app)


@app.cli.command()
def freeze():
    freezer.freeze()


@app.route('/leistungen/', methods=['GET'])
def leistungen():
    return render_template('leistungen.html')


@app.route('/kontakt/', methods=['GET'])
def kontakt():
    return render_template('kontakt.html')


@app.route('/kontakt/architektin-eichler.vcf', methods=['GET'])
def vcard():
    with open(Path(app.root_path, 'resources', 'vcard-photo.jpg'), mode='rb') as f:
        photo = base64.encodebytes(f.read())
    return Response(
        render_template('architektin-eichler.vcf', photo=photo.decode()),
        content_type='text/vcard'
    )


@app.route(app.config['PHOTO_URL_PATH'], methods=['GET'])
def foto():
    return send_file(
        Path(app.root_path, 'resources', 'vcard-photo.jpg'),
        mimetype='image/jpeg'
    )


@app.route('/galerie/', methods=['GET'])
def galerie():
    links = []
    for pic in os.scandir(app.config['PICTURE_SRC']):
        if pic.is_file():
            im = Image.open(pic.path)
            thumb = ImageOps.fit(im, (100, 100))
            thumb.save(app.config['THUMB_DIR'] / pic.name, "JPEG", optimize=True)
            if pic.name in app.config['ALREADY_COMPRESSED_PICTURES']:
                copyfile(pic.path, app.config['PICTURE_DIR'] / pic.name)
            else:
                im.thumbnail((800, 600))
                im.save(app.config['PICTURE_DIR'] / pic.name, "JPEG", optimize=True)
            links.append((
                url_for('static', filename=app.config['PICTURE_LOC'] / pic.name),
                url_for('static', filename=app.config['THUMB_LOC'] / pic.name)
            ))
    return render_template('galerie.html', links=links)


@app.route('/impressum/', methods=['GET'])
def impressum():
    return render_template('impressum.html')


@app.route('/', methods=['GET'])
def index():
    logo = Path(app.root_path, 'resources', 'vcard-photo.jpg')
    im = Image.open(logo)
    thumb = ImageOps.fit(im, (200, 200))
    thumb.save(Path(app.static_folder) / "logo.png", "PNG", optimize=True)
    return render_template('index.html')
