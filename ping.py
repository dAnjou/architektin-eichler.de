import sys
import requests

urls = [
    "https://architektineichler.de",
    "https://www.architektineichler.de",
    "https://architektin-eichler.de",
    "https://www.architektin-eichler.de",
    # have to check with DF why umlaut domains stopped working
    # "https://architekturbüro-eichler.de",
    # "https://www.architekturbüro-eichler.de",
]
exit_code = 0

for url in urls:
    try:
        response = requests.get(url=url)
        response.raise_for_status()
        print(f"{url}: {response.status_code}")
    except Exception as e:
        exit_code = 1
        print(f"{url}: {e}")

sys.exit(exit_code)
