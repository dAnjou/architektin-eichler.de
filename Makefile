setup:
	pip install --no-cache-dir -r requirements.txt
	flask --version

freeze: setup
	rm -rf public
	FLASK_APP=app.py flask freeze

run: freeze
	python -m http.server 8080 --bind 127.0.0.1 --directory public
